<?php

namespace Drupal\a11yproject_checklist\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The A11YProjectChecklist service.
 */
class A11yProjectChecklist {

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $httpClient, Messenger $messenger, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $httpClient;
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Gets the A11y Project Checklist from JSON and converts to PHP.
   *
   * @return string
   *   The checklist in PHP.
   */
  public function getA11yProjectChecklist(): array {
    // Where the checklist lives.
    $endpoint = 'https://raw.githubusercontent.com/a11yproject/a11yproject.com/main/src/_data/checklists.json';

    // Set some request options.
    $options = [];
    $options = [
      'connect_timeout' => 30,
    ];

    try {
      $client = $this->httpClient;
      $request = $client->request('GET', $endpoint, $options);
      if ($request->getStatusCode() == 200) {
        // Get the JSON.
        $checklist_json = $request->getBody()->getContents();

        // Convert it to PHP.
        $a11yproject_checklist = Json::decode($checklist_json);

        // Return the items.
        if (!is_array($a11yproject_checklist)) {
          return [];
        }
        return $a11yproject_checklist;
      }
    }
    catch (ConnectException $e) {
      // Connection exception.
      $this->loggerFactory->get('a11yproject_checklist')->error($e->getMessage());
      $this->messenger->addError($e->getMessage());
    }
    catch (RequestException $e) {
      // Request exception.
      $this->loggerFactory->get('a11yproject_checklist')->error($e->getMessage());
      $this->messenger->addError($e->getMessage());
    }

    // If error or status was not 200, return an empty string or handle error.
    return [];
  }

}
