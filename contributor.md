# Contributing
First off, thanks for taking the time to contribute!

#### Table Of Contents

[Code of Conduct](#code-of-conduct)

[Get an Answer to a Question](#get-an-answer-to-a-question)

[Prerequisites](#prerequisites)
  * [Drupal](#drupal)
  * [Patterns & Components](#Patterns-&-Components)
  * [A11Y Project Checklist Entry Points and Classes](#A11y project-checklist-entry-points-and-classes)

[How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Your First Code Contribution](#your-first-code-contribution)
  * [Pull Requests](#pull-requests)

[Styleguides](#styleguides)
  * [Git Commit Messages](#git-commit-messages)
  * [JavaScript Styleguide](#javascript-styleguide)
  * [PHP Styleguide](#php-styleguide)

## Code of Conduct
Please follow the [Drupal Code of Conduct](https://www.drupal.org/dcoc).

## Get an Answer to a Question

> **Note:** Please don't file an issue to ask a question. You'll get faster results by using the resources below.

You can join the Drupal Slack team:

* [Join the Drupal Slack Team](https://drupalslack.herokuapp.com/)
    * Even though Slack is a chat service, sometimes it takes several hours for community members to respond &mdash; please be patient!
   
    * There are many channels available, check the channel list for a relevance space.

## Prerequisites

### Drupal
A11Y Project Checklist is first and foremost a Drupal module. See the [Getting Involved Guide](https://www.drupal.org/contribute/development) for a step-by-step for contributing to Drupal.

### Patterns & Components
It's important to make a website accessible. This checklist uses the [Web Content Accessibility Guidelines WCAG]
(https://www.w3.org/WAI/standards-guidelines/wcag/) as a
reference point.

See the [README](https://git.drupalcode.org/project/a11yproject_checklist/-/blob/1.0.x/README.md) for basic definitions.

### A11 Y Project Checklist Entry Points and Classes

Drupal documents functionality through change records, which you can search at [https://www.drupal.org/list-changes/drupal](https://www.drupal.org/list-changes/drupal).

While most A11 y Project checklist files are self-explanatory:

 - **[a11yproject_checklist.info.yml](a11yproject_checklist.info.yml)**, **[Library](https://git.drupalcode.org/project/a11yproject_checklist/-/tree/1.0.x/src)** allow the documentation of accessibility best practices using A11Y standards.

 - **[a11yproject_checklist.services.yml](https://git.drupalcode.org/project/a11yproject_checklist/-/blob/1.0.x/a11yproject_checklist.services.yml)** configures working service and checklist.

- **[Templates](https://git.drupalcode.org/project/a11yproject_checklist/-/tree/1.0.x/templates)** is the default theme implementation for the A11Y Project Checklist form.

- **[a11yproject_checklist.install](https://git.drupalcode.org/project/a11yproject_checklist/-/blob/1.0.x/a11yproject_checklist.install)** allows to install, update and uninstall functions for the a11yproject_checklist module.

- **[a11yproject_checklist.module](https://git.drupalcode.org/project/a11yproject_checklist/-/blob/1.0.x/a11yproject_checklist.module)** uses best practices to check for a11y best practices.

## How Can I Contribute?
### Reporting Bugs

This section guides you through submitting a bug report. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

Bugs are tracked as [Drupal.org issues](https://www.drupal.org/project/a11yproject_checklist).

> **Note:** If you find a **Closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

#### Before Submitting A Bug Report

* **Perform a cursory search on [Drupal.org](https://www.drupal.org/project/a11yproject_checklist/issues/3267058)** to see if the problem has already been reported. If it has **and the issue is still open**, add a comment to the existing issue instead of opening a new one.

#### How Do I Submit A (Good) Bug Report?

Explain the problem and include additional details to help maintainers reproduce the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. For example, start by explaining how you installed a11yproject_checklist, e.g. which command exactly you used in the terminal. When listing steps, **don't just say what you did, but explain how you did it**. For example, if you moved the cursor to the end of a line, explain if you used the mouse, or a keyboard shortcut, and if so which one?
* **Provide specific examples to demonstrate the steps**. Include links to files or GitHub projects, or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use [Markdown code blocks](https://help.github.com/articles/markdown-basics/#multiple-lines).
* **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast) or [this tool](https://github.com/GNOME/byzanz) on Linux.
* **If you're reporting that a11yproject_checklist crashed**, include a crash report with a stack trace from PHP.  Include the crash report in the issue in a \<pre\>\<code\> block, an attachment, or put it in a [gist](https://gist.github.com/) and provide a link to that gist.
* **For performance or memory-related problems**, include a [profile capture](https://blackfire.io/) with your report.
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new version) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in an older version?** What's the most recent version in which the problem doesn't happen? 
* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.
* For files-system related files (e.g. opening and editing files), **does the problem happen for all files and projects or only some?** Does the problem happen only when working with local or remote files (e.g. on network drives), with files of a specific type (e.g. only JavaScript or Python files), with large files or files with very long lines, or with files in a specific encoding? Is there anything else special about the files you are using?

Include details about your configuration and environment:

* **Which version are you using?**
* **What's the name and version of the OS you're using**?
* **Which Drupal modules do you have installed?** You can get that list by running `drush pm-list`.

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion, including 
completely new features and minor improvements to existing functionality. 
Following these guidelines helps maintainers and the community understand your 
suggestion and find related suggestions.

Before creating enhancement suggestions, please check [this list
(#before-submitting-an-enhancement-suggestion) as you might find out that you 
don't need to create one. When you are creating an enhancement suggestion, 
please [include as many details as possible)
(#how-do-i-submit-a-good-enhancement-suggestion). Include the steps that you 
imagine you would take if the feature you're requesting existed.

#### Before Submitting An Enhancement Suggestion

* **Check if there's already [a module](https://drupal.org/project/modules) 
which provides that enhancement.**
* **Perform a cursory search on [Drupal.org](https://www.drupal.org/project/a11yproject_checklist/issues/3267058)** to see if the enhancement has already been 
suggested. If it has, add a comment to the existing issue instead of opening 
a new one.

#### How Do I Submit A (Good) Enhancement Suggestion?

Enhancement suggestions are tracked as [Drupal.org issues](https://www.drupal.org/project/a11yproject_checklist/issues/3267058). Create an issue and provide the 
following information:

* **Use a clear and descriptive title** for the issue to identify the 
suggestion.
* **Provide a step-by-step description of the suggested enhancement** in as 
many details as possible.
* **Provide specific examples to demonstrate the steps**. Include copy/
pasteable snippets which you use in those examples, as \<code\> blocks.
* **Describe the current behavior** and **explain which behavior you expected 
to see instead** and why.
* **Include screenshots and animated GIFs** which help you demonstrate the 
steps or point out the part which the suggestion is related to. You can use 
[this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and 
Windows, and [this tool](https://github.com/colinkeenan/silentcast) or 
[this tool](https://github.com/GNOME/byzanz) on Linux.
* **Explain why this enhancement would be useful** to most users and isn't 
something that can or should be implemented as another module.
* **List some other text editors or applications where this enhancement 
exists.**
* **Specify which version you're using.**
* **Specify the name and version of the OS you're using.**

### Your First Code Contribution

Unsure where to begin contributing? You can start by looking through these `novice` and `help-wanted` issues:

* [Novice issues][novice] - issues which should only require a few lines of code, and a test or two.
* [Help wanted issues][help-wanted] - issues which should be a bit more involved than `novice` issues.

#### Local development

You can install the A11 Y Project Checklist module as you would normally install a
contributed Drupal module. [Visit Drupal Installing Modules] (https://www.drupal.org/node/1897420) for further
information.

1. Navigate to Administration 
2. Extend and enable the module.
The checklist can be accessed via https://admin/config/development/a11y-project-checklist.
3. Review checklist items against your site.
4. Check the item once verified and click save.
5. Verify your username and date are now saved next to the checked item.
6. Optional: Export configuration to save the completed items to code.

Visit the A11 Y Project Checklist project page [for more information about this repository](https://www.drupal.org/project/a11yproject_checklist).

## Styleguides

### Git Commit Messages

[Use the Drupal.org Commit Guidelines](https://www.drupal.org/node/52287) (even if the module maintainers don't!)

### JavaScript Styleguide

[Use the Drupal.org JS Coding Standards](https://www.drupal.org/docs/develop/standards/javascript/javascript-coding-standards).

### PHP Styleguide

- [Use the Drupal.org PHP Coding Standards](https://www.drupal.org/docs/develop/standards/coding-standards).
- [Run Coder Sniffer](https://www.drupal.org/docs/contributed-modules/code-review-module/installing-coder-sniffer).
