# A11Y Project Checklist

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This checklist is provided by The [A11Y Project](https://www.a11yproject.com/),
[APLv2](https://www.apache.org/licenses/LICENSE-2.0). Please visit their site
and check out the awesome resources they provide to help make the web more
accessible to all. The module allows the documentation of A11Y best practices.
A11Y is an abbreviation for "accessibility" and focuses on making websites
more accessible.

This checklist uses the [Web Content Accessibility Guidelines (WCAG)]
(https://www.w3.org/WAI/standards-guidelines/wcag/) as a
reference point. WCAG is a shared standard for web content accessibility
for individuals, organizations, and governments.

 * For the description of the module visit:
   https://www.drupal.org/project/a11yproject_checklist

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/a11yproject_checklist


REQUIREMENTS
------------

This module requires the following module:

 * [Checklist API](https://www.drupal.org/project/checklistapi)


INSTALLATION
------------

Install the A11Y Project Checklist module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

  1. Navigate to Administration > Extend and enable the module.
  2. The checklist can be accessed via https://<site>/admin/config/development/a11y-project-checklist.
  2. Review checklist items against your site.
  3. Check the item once verified and click save.
  4. Verify your username and date are now saved next to the checked item.
  5. Optional: Export configuration to save the completed items to code.


MAINTAINERS
-----------

 * Jim Birch (thejimbirch) - https://www.drupal.org/u/thejimbirch

Supporting organization:

 * Kanopi Studios - https://www.drupal.org/kanopi-studios
