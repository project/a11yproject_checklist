<?php

namespace Drupal\Tests\a11yproject_checklist\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality of the A11y Project Checklist module.
 *
 * @group a11yproject_checklist
 */
class A11yProjectChecklistTests extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Module(s) for core functionality.
    'block',
    'field',
    'field_ui',
    'node',
    'views',
    'views_ui',

    // Contrib modules.
    'checklistapi',

    // This custom module.
    'a11yproject_checklist',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Set the front page to "node".
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);
  }

  /**
   * Make sure the site still works.
   */
  public function testTheSiteStillWorks() {
    // Load the front page.
    $this->drupalGet('<front>');

    // Confirm that the site didn't throw a server error or something else.
    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Checks if the A11Y Project Checklist loads.
   */
  public function testA11yProjectChecklistLoads() {
    // Create a user with the right permission and log in.
    $permissions = [
      'access administration pages',
      'administer site configuration',
      'edit a11yproject_checklist checklistapi checklist',
    ];
    $account = $this->drupalCreateUser($permissions);
    $this->drupalLogin($account);

    // Navigate to the checklist.
    $this->drupalGet('admin/config/development/a11y-project-checklist');

    // Confirm that the site didn't throw a server error or something else.
    $this->assertSession()->statusCodeEquals(200);

    // Confirm that the front page contains the standard text.
    $this->assertSession()->pageTextContains('Content is the most important part of your site.');

  }

  /**
   * Checks if we can check and save an item.
   */
  public function testA11yProjectChecklistItem() {
    // Create a user with the right permission and log in.
    $permissions = [
      'access administration pages',
      'administer site configuration',
      'edit a11yproject_checklist checklistapi checklist',
    ];
    $account = $this->drupalCreateUser($permissions);
    $this->drupalLogin($account);

    // Navigate to the checklist.
    $this->drupalGet('admin/config/development/a11y-project-checklist');

    // Confirm that the site didn't throw a server error or something else.
    $this->assertSession()->statusCodeEquals(200);

    // Submit form with the first item checked.
    $translateSave = t('Save');
    $this->submitForm([
      'edit-checklistapi-content-use-plain-language' => '1',
    ], $translateSave->render());
    // Verify the item has been saved.
    $this->assertSession()->pageTextContains('A11Y Project checklist progress has been saved. 1 item changed. ');
  }

}
